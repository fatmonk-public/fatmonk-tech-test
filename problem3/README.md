**#Clone from git in order to view the multi-select options.**

* After that install 
 ``` npm i npm-multiselect```
 custom package to add all the dependencies.
* Then 
    ```npm start```
    to launch the project as the package is ready.
* Now, the project is ready to go.
* the custom package is created for enabling multi-select options all feature.

**#For existing project, ``` --typescript ``` needs to be enabled when the project is created.**
 
*Step 1 : Install 
```npm i npm-multiselect``` custom package to add all the dependencies.*


*Step 2 : In 
``` App.tsx ``` add following code:*


	
	import { MultiSelectComponent  } from '@syncfusion/ej2-react-dropdowns';
	import * as React from 'react';
	import * as ReactDOM from "react-dom";

	export default class App extends React.Component<{}, {}> {
		public render() {
			return (
		<MultiSelectComponent  id='mtselement'/>
			);
		}
	}

	ReactDOM.render(<App />, document.getElementById('sample'));

	

*Step 3 : index.tsx needs to be customized as below:*


	

		import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';
		import * as React from 'react';
		import * as ReactDOM from 'react-dom';

		export default class App extends React.Component<{}, {}> {
		
		  private sportsData: string[] = ['Badminton', 'Cricket', 'Football', 'Golf', 'Tennis','a','b','c','d','e','f'];

			public render() {
				return (
					<MultiSelectComponent id="checkbox" dataSource={this.sportsData}
						placeholder="Select game" mode="CheckBox">
						<Inject services={[CheckBoxSelection]} />
					</MultiSelectComponent>
				);
			}
		}
		ReactDOM.render(<App />, document.getElementById('sample'));


	

*Step 4 : Add following links in index.html as the feature has been built by using material UI.*



	<link href="//cdn.syncfusion.com/ej2/ej2-base/styles/material.css" rel="stylesheet" />
    <link href="//cdn.syncfusion.com/ej2/ej2-react-inputs/styles/material.css" rel="stylesheet" />
    <link href="//cdn.syncfusion.com/ej2/ej2-react-dropdowns/styles/material.css" rel="stylesheet" />
    <link href="//cdn.syncfusion.com/ej2/ej2-react-buttons/styles/material.css" rel="stylesheet" />


	

****These file is needed in order to function the tsx files properly:****
	
	- tsconfig.json
	- src/app.tsx
	