import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';
import * as React from 'react';
import * as ReactDOM from 'react-dom';

export default class App extends React.Component<{}, {}> {
    // define the array of string
  private sportsData: string[] = ['Badminton', 'Cricket', 'Football', 'Golf', 'Tennis','a','b','c','d','e','f'];

    public render() {
        return (
            // specifies the tag for render the MultiSelect component
            <MultiSelectComponent id="checkbox" dataSource={this.sportsData}
                placeholder="Select game" mode="CheckBox">
                <Inject services={[CheckBoxSelection]} />
            </MultiSelectComponent>
        );
    }
}
ReactDOM.render(<App />, document.getElementById('sample'));
