
import { MultiSelectComponent  } from '@syncfusion/ej2-react-dropdowns';
import * as React from 'react';
import * as ReactDOM from "react-dom";

export default class App extends React.Component<{}, {}> {
  public render() {
    return (
       // specifies the tag for render the MultiSelect component
      <MultiSelectComponent  id='mtselement'/>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('sample'));
