package com.rakib.diboshfood;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Spinner spinnerFoodItem;
    Spinner spinnerRestaurantItem;
    Spinner spinnerPriceItem;
    Spinner spinnerRatingItem;
    Spinner spinnerExplore;

    private ArrayList<Item> mFoodTypeList;
    private ArrayList<Item> mRestaurantTypeList;
    private ArrayList<Item> mPriceList;
    private ArrayList<Item> mRatingList;
    private ArrayList<Item> mExploreList;

    private Adapter mFoodTypeAdapter;
    private Adapter mRestaurantTypeAdapter;
    private Adapter mPriceAdapter;
    private Adapter mRatingAdapter;
    private Adapter mExploreAdapter;

    int foodItemCode = 0;
    int restaurantItemCode = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        spinnerFoodItem = findViewById(R.id.spinnerFood);
        spinnerRestaurantItem = findViewById(R.id.spinnerRestaurant);
        spinnerPriceItem = findViewById(R.id.spinnerPrice);
        spinnerRatingItem = findViewById(R.id.spinnerRating);
        spinnerRatingItem = findViewById(R.id.spinnerExplore);

        // For Food Type
        initListFoodType();

        mFoodTypeAdapter = new Adapter(this, mFoodTypeList);
        spinnerFoodItem.setAdapter(mFoodTypeAdapter);

        spinnerFoodItem.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                foodItemCode = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getApplicationContext(), "Please select a country.", Toast.LENGTH_LONG).show();
            }
        });

        // For Restaurant Type
        initListRestaurantType();

        mRestaurantTypeAdapter = new Adapter(this, mRestaurantTypeList);
        spinnerRestaurantItem.setAdapter(mRestaurantTypeAdapter);

        spinnerRestaurantItem.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                foodItemCode = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getApplicationContext(), "Please select a country.", Toast.LENGTH_LONG).show();
            }
        });

        // For Restaurant Price
        initListPrice();

        mPriceAdapter = new Adapter(this, mPriceList);
        spinnerPriceItem.setAdapter(mPriceAdapter);

        spinnerPriceItem.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                foodItemCode = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getApplicationContext(), "Please select a country.", Toast.LENGTH_LONG).show();
            }
        });

        // For Restaurant Type
        initListRestaurantType();

        mRestaurantTypeAdapter = new Adapter(this, mRestaurantTypeList);
        spinnerRestaurantItem.setAdapter(mRestaurantTypeAdapter);

        spinnerRestaurantItem.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                foodItemCode = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getApplicationContext(), "Please select a country.", Toast.LENGTH_LONG).show();
            }
        });

        // For Restaurant Type
        initListRestaurantType();

        mRestaurantTypeAdapter = new Adapter(this, mRestaurantTypeList);
        spinnerRestaurantItem.setAdapter(mRestaurantTypeAdapter);

        spinnerRestaurantItem.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                foodItemCode = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getApplicationContext(), "Please select a country.", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void initListPrice() {
    }

    private void initListFoodType() {
        mFoodTypeList = new ArrayList<>();
        mFoodTypeList.add(new Item("Select Food Type"));
        mFoodTypeList.add(new Item("Burger"));
        mFoodTypeList.add(new Item("Pasta"));
        mFoodTypeList.add(new Item("Pizza"));
    }

    private void initListRestaurantType() {
        mRestaurantTypeList = new ArrayList<>();
        mRestaurantTypeList.add(new Item("Select Restaurant Type"));
        mRestaurantTypeList.add(new Item("Bangladeshi"));
        mRestaurantTypeList.add(new Item("Indian"));
        mRestaurantTypeList.add(new Item("Chinese"));
    }
}
