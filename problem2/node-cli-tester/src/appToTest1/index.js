#!/usr/bin/env node

var fs = require('fs');
var program = require('commander');
var data = '';

 program
  .arguments('<file> <file>')
  .action(function(input,output) {
  	var readStream = fs.createReadStream(''+input, 'utf8');
  	var wrStream= fs.createWriteStream(__dirname+'/test/'+output, 'utf8');
  	readStream.on('data', function(chunk) { 
  		data=chunk+"done";
  			wrStream.write(data); 
  	}).on('end', function() {
  		console.log('File write done!');
  	});
    //console.log('input file: %s \noutputfile: %s',input,output);
  })
  .parse(process.argv);