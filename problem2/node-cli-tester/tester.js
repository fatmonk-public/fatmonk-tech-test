#!/usr/bin/env node
var cmd=require('node-cmd');
var fs = require('fs');

var str='';
var args=process.argv.slice(2);
for(let i=0;i<args.length;i++)
{
  str=str+' '+args[i];
}
console.log('\nInitializing tester...\n\n');

cmd.run(str);


var readOutput = fs.createReadStream('src/appToTest1/test/output.txt', 'utf8');
var readGivenSet = fs.createReadStream(''+args[args.length-1],'utf8');

readOutput.on('data', function(chunk) {
	readGivenSet.on('data',function(chunk2){
		if(chunk==chunk2){
			console.log('CLi tester: Output matched with the given output file.\n')
		}
		else{
			throw new Error('CLi tester: Output did not match with the given output file.\n');
		}
	}).on('end',function(){

	})
  
  }).on('end', function() {
  		console.log('my-cli is working!\n');
  	});