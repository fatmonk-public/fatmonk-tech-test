
process.env.NODE_ENV = 'test'

var supertest = require("supertest");
var should = require("should");
var fs= require("fs")
/*var jsonData = {};
fs.readFile( __dirname+"/test/fixtures/"+"9.json",'utf8',function(err, data){
  jsonData=data;
   console.log(jsonData);
})*/
var data=[{
  "port": 8080,
  "endpoint": "/api/users", 
  "request": {
    "params": {
      "name":"fatmonk",
      "id":"1"
    },
    "body": {
      "str": "request body."
    }
  },
  "response": {
    "name": "fatmonk",
    "id":"1"
  }
},{
  "port": 8080,
  "endpoint": "/api/users", 
  "request": {
    "params": {
      "name":"fatmonk",
      "id":"2"
    },
    "body": {
      "str": "request body."
    }
  },
  "response": {
    "name": "fatmonk",
    "id":"2"
  }
},{
  "port": 8080,
  "endpoint": "/api/users", 
  "request": {
    "params": {
      "name":"fatmonk",
      "id":"3"
    },
    "body": {
      "str": "request body."
    }
  },
  "response": {
    "name": "fatmonk",
    "id":"3"
  }
},{
  "port": 8080,
  "endpoint": "/api/users", 
  "request": {
    "params": {
      "name":"fatmonk",
      "id":"4"
    },
    "body": {
      "str": "request body."
    }
  },
  "response": {
    "name": "fatmonk",
    "id":"4"
  }
},{
  "port": 8080,
  "endpoint": "/api/users", 
  "request": {
    "params": {
      "name":"fatmonk",
      "id":"5"
    },
    "body": {
      "str": "request body."
    }
  },
  "response": {
    "name": "fatmonk",
    "id":"5"
  }
}]
for(i in data)
{
var server = supertest.agent('http://localhost:'+JSON.stringify(data[i].port));

describe(`test case: ${i} ...`,function(){
  it("should return 'name' and 'id' ",function(done){
    server
    .post(data[i].endpoint)
    .send(data[i].request.params)
    .expect("Content-type",/json/)
    .expect(200)
    .end(function(err,res){
      res.status.should.equal(200);
      res.body.error.should.equal(false);
      res.body.data.should.equal(data[i].response.name+" "+data[i].response.id);
      done();
    });
  });

});
}
